module.exports = {
  registerRoutes: function(app) {
    app.get('/', this.home);
  },

  home: function(req, res, next) {
    res.render('home', {'title':'AirCompanies Home'});
  }
};
