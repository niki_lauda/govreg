var AirCompany = require('../models/aircompany');
var range = require('node-range');

module.exports = {
  registerRoutes: function(app){
    app.get('/mvc/new-company', this.getFormCompany);
    app.post('/mvc/add-company', this.addCompany);
    app.get('/mvc/company/:page', this.listCompanies);
    app.get('/mvc/company', this.listCompanies);
    app.post('/mvc/del-company', this.deleteCompany);
    app.post('/mvc/edit-company', this.updateCompany);
  },

  getFormCompany: function(req, res, next){
    res.render('new-company', {"title":"Add new Air Company"});
  },

  addCompany: function(req, res, next){
    //TODO:add validation logic for a modelView
    var company = new AirCompany(req.body);
    company.save(function(err){
      if(err){
        console.log(err);
        return;
      }
      res.redirect(303, '/mvc/company');
    });
  },

  listCompanies: function(req, res, next){
    AirCompany.find({}, function(err, companies){
      var sliced = [];
      var page = parseInt(req.params.page);
      if(typeof req.params.page === "undefined" || typeof page != "number"){
        page = 0;
        if(companies.length < 5)
          sliced = companies;
        else
          sliced = companies.slice(0, 5);
      }
      else if (Math.ceil(companies.length/5) > page){
        if(companies.length < page * 5 + 5){
          sliced = companies.slice(page * 5, companies.length);
        }
        else{
          sliced = companies.slice(page * 5, page * 5 + 5);
        }
      }
      else{
        return next();
      }

      res.render('company', {
        "companies": sliced,
        "title":"Air Companies",
        'prev' : page - 1 >= 0 ? page - 1 : page,
        'next' : page + 1 < Math.ceil(companies.length/5) ? page + 1 : page
      });
    });
  },

  deleteCompany: function(req, res, next){
    if(req.xhr){
      //TODO: check if authorized
      AirCompany.remove({_id:req.body.id}, function(err){
        if(err){
        }
        else{
          res.contentType('json');
          res.send(JSON.stringify({success: true}));
        }
      });
    }
  },

  updateCompany: function(req, res, next){
    if(req.xhr){
      AirCompany.update({_id:req.body.id}, {$set:req.body}, function(err){
        if(err){
          console.log(err);
          return;
        }
        AirCompany.find({_id:req.body.id}, function(err, companies){
          if(err){
            console.log(err);
            return;
          }
          res.contentType('json');
          if(companies.length > 0){
            res.send(JSON.stringify({success:true, childs:companies[0]}));
            return;
          }
          res.send(JSON.stringify({success:false}));
        });
      });
    }
  },
};
