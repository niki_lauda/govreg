var AirCompany = require('../models/aircompany');

module.exports = {
  registerRoutes: function(app){
    app.get('/api', this.api);
    app.get('/api/company', this.companyAll);
    app.get('/api/company/:id', this.companyById);
    app.put('/api/company/:id', this.companyUpdate);
    app.delete('/api/company/:id', this.companyDelete);
    app.post('/api/company', this.companyCreate);
  },

  api: function(req, res, next){
    res.render('api', {'title':'RESTful API'});
  },

  companyAll: function(req, res, next){
      AirCompany.find(function(err, companies){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }
      res.send(JSON.stringify(companies, null, 3));
    });
  },

  companyById: function(req, res, next){
    AirCompany.findById(req.params.id, function(err, company){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }
      res.send(JSON.stringify(company, null, 3));
    });
  },

  companyUpdate: function(req, res, next){
    AirCompany.findById(req.params.id, function(err, company){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }

      for(var attribute in req.body){
        if(req.body[attribute] !== undefined && company[attribute] !== undefined){
          company[attribute] = req.body[attribute];
          company.save(function(err){
            if(err){
              res.send(JSON.stringify({"status" : "failed"}, null, 3));
              return;
            }
          });
        }
      }
      res.send(JSON.stringify({"status" : "success"}, null, 3));
    });
  },

  companyDelete: function(req, res, next){
    AirCompany.remove({_id:req.params.id}, function(err){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }
      res.send(JSON.stringify({"status" : "success"}, null, 3));
    });
  },

  companyCreate: function(req, res, next){
    var company = new AirCompany(req.body);
    company.save(function(err){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }
      res.send(JSON.stringify({"status" : "success"}, null, 3));
    });
  },

  sendErr: function(err, msg){
    return res.send(JSON.stringify({
      "Error" : msg,
      "Details" : JSON.stringify(err)}, null, 3));
  }
};
