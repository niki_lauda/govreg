var fs = require('fs');
var parse = require('csv-parse');
var inputFile = 'data.csv';
var fieldNames = ['decisionDate', 'authorityCode', 'authorityType', 'carrierCodeName', 'destCountry', 'route',
'flightFrequency','authorityStartDate', 'confirmationBase', 'confirmationDate'];

module.exports = function(callback){
  var parser = parse({delimiter:','}, function(err, data){
    var objects = [];
    for(var i = 0; i < data.length; i++){
      if(data[i].length > 0 && data[i].length === fieldNames.length){
        var object = {};
        for(var j = 0; j < data[i].length;  j++){
          object[fieldNames[j]] = data[i][j];
        }
        objects.push(object);
      }
    }
    callback(objects);
  });

  fs.createReadStream(inputFile).pipe(parser);
}
