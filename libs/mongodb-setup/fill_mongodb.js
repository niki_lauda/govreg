var mongoose = require('mongoose');
var config = require('../../config');
var AirCompany = require('../../models/aircompany');
var parser = require('./dataparser');

mongoose.connect(config.mongoDB.mongodbUri);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

  parser(function(data){
    var savesPending = data.length;

    var saveFinished = function(err, company){
      if(err){
        return console.error(err);
      }
      savesPending--;
      if(savesPending === 0){
        mongoose.disconnect();
      }
    };

    for(var i = 0; i < data.length ; i++){
      var company = new AirCompany(data[i]);
      company.save(saveFinished);
    }
  });
});
