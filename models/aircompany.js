var mongoose = require('mongoose');

var aircompanySchema = mongoose.Schema({
  decisionDate: String,
  authorityCode: Number,
  authorityType: String,
  carrierCodeName: String,
  destCountry: String,
  route: String,
  flightFrequency: String,
  authorityStartDate: String,
  confirmationBase: String,
  confirmationDate: String,
});

var AirCompany = mongoose.model('airCompany', aircompanySchema);
module.exports = AirCompany;
