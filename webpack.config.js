var webpack = require('webpack');
var debug = process.env.NODE_ENV !== "production";

module.exports = {
    context: __dirname,
    devtool: debug ? "inline-sourcemap" : null,
    resolve: {
        extensions: ['', '.js', '.ts']
    },
    entry:
    {
        tests: "./spa/public/qa/requiredall.js",
        client: "./spa/js/client.js"
    },
    ignore: 'node_modules',
    module:{
        noParse: /node_modules\/json-schema\/lib\/validate\.js/,
        loaders:[
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query:{
                    presets:['react', 'es2015', 'stage-0'],
                    plugins:['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
                }
            }
        ]
    },
    output:{
        path: __dirname + "/spa/dist",
        filename: "[name].min.js"
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({mangle:false, sourcemap: false}),
        new webpack.IgnorePlugin(/jsdom$/)
    ]
};