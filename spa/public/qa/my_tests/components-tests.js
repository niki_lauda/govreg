var ReactDOM = require('react-dom');

var Footer = require("../../../js/components/Footer").default;
var Loader = require("../../../js/components/Loader").default;
var Navbar = require("../../../js/components/Navbar").default;
var AirCompany = require("../../../js/components/AirCompany").default;
var ListItems = require("../../../js/pages/ListItems").default;

var TestUtils = require("react-addons-test-utils");
var assert = require("chai").assert;
var React = require("react");
var chai = require('chai');
var chai = require('chai'), chaiHttp = require('chai-http');
chai.use(chaiHttp);
var url = require('../../../js/apiRoute').default;


describe('ReactJS Components testing', function () {
    it('Footer signature must be rendered', function () {
        const copyrightSign = '\u00a9';
        const footerSignature = 'Copyright ' + copyrightSign + ' Yevhenii Maliavka';
        var renderedComponent = TestUtils.renderIntoDocument(<Footer/>);
        var paragraph = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'p');
        assert.equal(paragraph.innerHTML, footerSignature);
    });

    it('Loader component must have class "loader"', function () {
        var renderedComponent = TestUtils.renderIntoDocument(<Loader/>);
        var div = TestUtils.findRenderedDOMComponentWithClass(renderedComponent, 'loader');
        assert.equal('loader', div.className);
    });

    describe('Navbar testing', function () {
        it('button click should collapse the navbar', function () {
            var renderedComponent = TestUtils.renderIntoDocument(<Navbar location="/"/>);
            var buttonReact = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'button');
            var nav = TestUtils.scryRenderedDOMComponentsWithClass(renderedComponent, 'navbar-collapse collapse');
            assert(nav.length === 1);
            TestUtils.Simulate.click(buttonReact);
            nav = TestUtils.scryRenderedDOMComponentsWithClass(renderedComponent, 'navbar-collapse collapse');
            assert(nav.length === 0);
        });

    });

    describe('ListItems Component testing', function () {
        this.timeout(10000);
        var response = null;
        var renderedComponent = null;

        before(function (done) {
            renderedComponent = TestUtils.renderIntoDocument(<ListItems/>);
            chai.request(url + '/company')
                .get("/")
                .end(function (err, res) {
                    response = res;
                    done();
                });
        });

        it('number of rendered AirCompany components should equal the number of objects from API Get', function () {
            //before statement is executed to get everything ready
            var lst = TestUtils.scryRenderedDOMComponentsWithClass(renderedComponent, 'company');
            assert.equal(lst.length, response.body.length);
        });

        it('list of elements should not contain zero components', function () {
            var lst = TestUtils.scryRenderedDOMComponentsWithClass(renderedComponent, 'company');
            assert.notEqual(lst.length, 0);
        });
    });

    describe('AirCompany Component testing', function () {
        var testObject = {
            decisionDate: "test_data_field",
            authorityCode: "111111111111111",
            authorityType: "test_data_field",
            carrierCodeName: "test_data_field",
            destCountry: "test_data_field",
            route: "test_data_field",
            flightFrequency: "test_data_field",
            authorityStartDate: "test_data_field",
            confirmationBase: "test_data_field",
            confirmationDate: "test_data_field"
        };

        var response = null;

        before(function(done){
            chai.request(url + '/company')
                .post("/")
                .send(testObject)
                .end(function (err, res) {
                    response = res;
                    done();
                });
        });

        it('AirCompany component should be populated with values after being mounted', function () {
            var renderedComponent = TestUtils.renderIntoDocument(<AirCompany data={testObject}/>);
            var dataFields = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'strong');
            dataFields.map(function(domObject){
                assert.equal(domObject.innerHTML, testObject[domObject.className])
            });
        });

        it('should get inputs appearing to edit on edit-button click', function () {
            var renderedComponent = TestUtils.renderIntoDocument(<AirCompany data={testObject}/>);
            //throws error if not the one button is found
            var btn = TestUtils.findRenderedDOMComponentWithClass(renderedComponent, 'edit-btn');
            renderedComponent.setState({edit:false});
            assert(!renderedComponent.state.edit);
            TestUtils.Simulate.click(btn);
            assert(renderedComponent.state.edit);
        });

        it('should hide inputs on cancel-button click', function () {
            var renderedComponent = TestUtils.renderIntoDocument(<AirCompany data={testObject}/>);
            //throws error if not the one button is found
            var btn = TestUtils.findRenderedDOMComponentWithClass(renderedComponent, 'cancel-btn');
            renderedComponent.setState({edit:true});
            assert(renderedComponent.state.edit);
            TestUtils.Simulate.click(btn);
            assert(!renderedComponent.state.edit);
        });

        it('should re-render input on change', function () {
            assert.equal(response.body.status, 'success');
            var renderedComponent = TestUtils.renderIntoDocument(<AirCompany data={testObject}/>);
            var inputs = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'input');
            if(inputs.length === 0){
                assert(false);
                return;
            }
            var input = inputs[0];
            var initialValue = renderedComponent.state.data[input.name];
            input.value = 'input_changed_test';
            TestUtils.Simulate.change(input);
            var changedValue = renderedComponent.state.data[input.name];
            assert.notEqual(initialValue, changedValue);
        });
    });

});

