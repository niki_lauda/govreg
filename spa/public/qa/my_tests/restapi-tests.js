var assert = require("chai").assert;
var chai = require('chai'), chaiHttp = require('chai-http');
chai.use(chaiHttp);
var url = require('../../../js/apiRoute').default;

describe('RESTful API Testing', function () {
    var test_object = {
        "decisionDate": "09.12.2015",
        "authorityCode": 1249,
        "authorityType": "test_default_api",
        "carrierCodeName": "test_default_api",
        "destCountry": "test_default_api",
        "route": "test_default_api",
        "flightFrequency": "test_default_api",
        "authorityStartDate": "09.12.2015",
        "confirmationBase": "Протокол засідання комісії ДАС №7 від 09.12.2015",
        "confirmationDate": "11.12.2015",
        "__v": 0
    };

    describe('GET Requests', function () {
        this.timeout(10000);
        var response = null;
        before(function (done) {
            chai.request(url + '/company')
                .get("/")
                .end(function (err, res) {
                    response = res;
                    done();
                })
        });
        it('should return Status Code 200 on a request', function () {
            //GET Request in describe:before() method;
            assert.equal(response.status, '200');
        });

        it('should return array of objects on a request ', function () {
            //GET Request in describe:before() method;
            assert.typeOf(response.body, 'Array');
        });
    });

    describe('POST Requests', function () {
        this.timeout(10000);
        it('should create object and get Status Code 200', function (done) {
            chai.request(url + '/company')
                .post("/")
                .send(test_object)
                .end(function (err, res) {
                    assert.equal(res.status, '200');
                    done();
                })
        });
    });

    describe('CREATE Requests via API', function () {
        this.timeout(10000);
        var response = null;
        before(function (done) {
            chai.request(url + '/company')
                .get("/")
                .end(function (err, res) {
                    response = res;
                    done();
                })
        });

        it('should get response Status Code 200', function () {
            assert.equal(response.status, '200');
        });

        it('fetched test object must be equal locally stored object', function (done) {
            response.body.map(function (object) {
                if (object.authorityType === 'test_default_api') {
                    assert(true);
                    done();
                }
            });
        });
    });

    describe('PUT requests', function () {
        this.timeout(10000);
        var response = null;
        before(function (done) {
            chai.request(url + '/company')
                .get("/")
                .end(function (err, res) {
                    response = res;
                    done();
                })
        });

        it('should change object via PUT reqeust', function (done) {
            response.body.map(function (object) {
                if (object.authorityType === 'test_default_api') {
                    object.authorityType = 'test_default_api_put';
                    chai.request(url + '/company')
                        .put("/" + object._id)
                        .send(object)
                        .end(function (err, res) {
                            assert(res.status, '200');
                            assert(res.body.status, 'success');
                            done();
                        });
                }
            });
        });

        it('fetched test object should match locally stored object after changes', function (done) {
            chai.request(url + '/company')
                .get("/")
                .end(function (err, res) {
                    res.body.map(function (object) {
                        if (object.authorityType === 'test_default_api_put') {
                            assert(true);
                        }
                    });
                    done();
                });
        });
    });

    describe('DELETE Requests', function () {
        this.timeout(10000);
        var response = null;
        before(function (done) {
            chai.request(url + '/company')
                .get("/")
                .end(function (err, res) {
                    response = res;
                    done();
                })
        });

        it('should delete created test objects', function (done) {
            response.body.map(function (object) {
                if (object.authorityType === 'test_default_api_put') {
                    chai.request(url + '/company')
                        .delete("/" + object._id)
                        .end(function (err, res) {
                            assert.equal(res.status, '200');
                            assert.equal(res.body.status, 'success');
                            done();
                        })
                }
            });

        });

        it('test object should not more exist', function (done) {
            chai.request(url + '/company')
                .get("/")
                .end(function (err, res) {
                    res.body.map(function (object) {
                        assert.notEqual(object.authorityType, 'test_default_api_put');
                    });
                    done();
                })
        });
    });


});