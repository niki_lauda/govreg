mocha.ui('tdd');

suite('Global Page Tests', function(){
    test('Page must have a valid title', function(){
        assert(document.title && document.title.match(/\S/) && document.title.toUpperCase() !== 'TODO');
    });

    test('Page must have a div with id "app" as space for ReactJS Application', function(){
        assert($('div[id="app"]').length);
    });
});
