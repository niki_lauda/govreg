import React from "react";

export default class Loader extends React.Component {
    render() {
        return (
            <div class="loader">
                <i class="fa fa-plane" aria-hidden="true"></i>
            </div>
        );
    }
}