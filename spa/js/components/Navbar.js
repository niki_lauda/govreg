import React from "react";
import {IndexLink, Link} from "react-router";

export default class Navbar extends React.Component{
    constructor(){
        super();
        this.state = {collapsed: true};
    }

    toggleCollapse(){
        const collapsed = !this.state.collapsed;
        this.setState({collapsed});
    }

    render(){
        const { location } = this.props;
        const { collapsed } = this.state;
        const listItems = location.pathname === "/" ? "active" : "";
        const newItem = location.pathname === "/new-item" ? "active" : "";
        const tests = location.pathname === "/tests" ? "active" : "";
        const visuality = location.pathname === "/visuality" ? "active" : "";
        const navClass = collapsed ? "collapse" : "";

        return(
          <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" onClick={this.toggleCollapse.bind(this)}>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                  </div>
              </div>
              <div class={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                        <li class={listItems}>
                            <IndexLink to="/" onClick={this.toggleCollapse.bind(this)}>List Items</IndexLink>
                        </li>
                      <li class={newItem}>
                          <Link to="new-item" onClick={this.toggleCollapse.bind(this)}>New Item</Link>
                      </li>

                      <li class={visuality}>
                          <Link to="visuality" onClick={this.toggleCollapse.bind(this)}>Visuality</Link>
                      </li>
                  </ul>
              </div>
          </nav>
        );
    }
}
