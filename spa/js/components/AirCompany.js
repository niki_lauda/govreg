import React from "react";
import * as companiesActions from "../actions/CompaniesActions";
import url from "../apiRoute";

export default class AirCompany extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: JSON.parse(JSON.stringify(props.data)),
            edit: false,
            classString: ""
        };
    }

    static getInitialState() {
        return {
            decisionDate: "",
            authorityCode: "",
            authorityType: "",
            carrierCodeName: "",
            destCountry: "",
            route: "",
            flightFrequency: "",
            authorityStartDate: "",
            confirmationBase: "",
            confirmationDate: ""
        };
    }

    handleEditClick() {
        this.setState({edit: true, classString: "form-control"});
    }

    handleDeleteClick() {
        const {_id} = this.props.data;
        companiesActions.deleteCompanyApi(url + '/company', _id);
    }

    handleAcceptClick() {
        this.setState({edit: false, classString: ""});
        companiesActions.updateCompanyApi(url + '/company', this.state.data);
    }

    handleCancelClick() {
        this.setState({data: AirCompany.getInitialState()});
        this.setState({data: JSON.parse(JSON.stringify(this.props.data)), edit: false, classString: ""});
    }

    handleFieldChange(e) {
        var object = {};
        object[e.target.name] = e.target.value;
        var data = this.state.data;
        data[e.target.name] = e.target.value;
        this.setState(data);
    }

    render() {
        const {
            _id, decisionDate, authorityCode, authorityType, carrierCodeName, destCountry, route, flightFrequency,
            authorityStartDate, confirmationBase, confirmationDate
        } = this.state.data;

        return (
            <div class="jumbotron company" style={{width:850, padding: 5}} id={_id}>
                <div class="icons" align="right">
                    <button class="edit-btn" onClick={this.handleEditClick.bind(this)}><i class="fa fa-pencil-square-o"></i></button>
                    <button class="del-btn" onClick={this.handleDeleteClick.bind(this)}><i class="fa fa-trash"></i></button>
                    <button class="cancel-btn" hidden={!this.state.edit} onClick={this.handleCancelClick.bind(this)}><i class="fa fa-ban"
                                                                                                     aria-hidden="true"></i>
                    </button>
                    <button class="accept-btn" hidden={!this.state.edit} onClick={this.handleAcceptClick.bind(this)}><i
                        class="fa fa-check"></i></button>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        Дата рішення про надання/зміну права:
                        <strong hidden={this.state.edit} class="decisionDate">{decisionDate}</strong>
                        <input type="text" name="decisionDate" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={decisionDate}></input>
                    </li>

                    <li class="list-group-item">
                        № права:
                        <strong hidden={this.state.edit} class="authorityCode">{authorityCode}</strong>
                        <input type="number" name="authorityCode" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={authorityCode}></input>
                    </li>
                    <li class="list-group-item">
                        Тип права:
                        <strong hidden={this.state.edit} class="authorityType">{authorityType}</strong>
                        <input type="text" name="authorityType" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={authorityType}></input>
                    </li>

                    <li class="list-group-item">
                        Назва і код ІКАО / ІАТА авіаперевізника:
                        <strong hidden={this.state.edit} class="carrierCodeName">{carrierCodeName}</strong>
                        <input type="text" name="carrierCodeName" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={carrierCodeName}></input>
                    </li>

                    <li class="list-group-item">
                        Країна призначення:
                        <strong hidden={this.state.edit} class="destCountry">{destCountry}</strong>
                        <input type="text" name="destCountry" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={destCountry}></input>
                    </li>

                    <li class="list-group-item">
                        Маршрут:
                        <strong hidden={this.state.edit} class="route">{route}</strong>
                        <input type="text" name="route" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString} placeholder={route}></input>

                    </li>

                    <li class="list-group-item">
                        Частота рейсів:
                        <strong hidden={this.state.edit} class="flightFrequency">{flightFrequency}</strong>
                        <input type="text" name="flightFrequency" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={flightFrequency}></input>

                    </li>

                    <li class="list-group-item">
                        Дата початку дії права:
                        <strong hidden={this.state.edit} class="authorityStartDate">{authorityStartDate}</strong>
                        <input type="text" name="authorityStartDate" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={authorityStartDate}></input>

                    </li>

                    <li class="list-group-item">
                        Підстава надання:
                        <strong hidden={this.state.edit} class="confirmationBase">{confirmationBase}</strong>
                        <input type="text" name="confirmationBase" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={confirmationBase}></input>

                    </li>

                    <li class="list-group-item">
                        Дата видачі:
                        <strong hidden={this.state.edit} class="confirmationDate">{confirmationDate}</strong>
                        <input type="text" name="confirmationDate" onChange={this.handleFieldChange.bind(this)}
                               hidden={!this.state.edit} className={this.state.classString}
                               placeholder={confirmationDate}></input>

                    </li>
                </ul>
            </div>
        );
    }
}