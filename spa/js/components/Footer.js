import React from "react";

export default class Footer extends React.Component {
    render() {
        return (
            <footer>
                <p class="footer-title">Copyright &copy; Yevhenii Maliavka</p>
            </footer>
        );
    }
}
