import dispatcher from "../dispatcher";
import $ from 'jquery';

export function accessApi(urlCall){
    $.getJSON(urlCall)
        .then(function(data) {
            dispatcher.dispatch({
                type:"RECEIVED_COMPANIES",
                context: data
            });
        });
}

export function saveCompanyApi(urlCall, data){
    $.post(urlCall, data)
        .done(function() {
            console.log("Saved");
            dispatcher.dispatch({
                type:"SAVED_COMPANIES",
                context: data
            });
        });
}

export function deleteCompanyApi(urlCall, id){
    $.ajax({
        url: urlCall + '/' + id,
        type: 'DELETE',
        success: function(){
            dispatcher.dispatch({
                type: "DELETED_COMPANY",
                context: id
            })
        },
        error: function(){
            console.log("ERROR");
        }
    });
}

export function updateCompanyApi(urlCall, company){
    $.ajax({
        url: urlCall + '/' + company._id,
        type: 'PUT',
        data: company,
        success: function(){
            dispatcher.dispatch({
                type: "UPDATED_COMPANY",
                context: company._id
            })
        },
        error: function(){
            console.log("ERROR");
        }
    });
}