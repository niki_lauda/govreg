import React from "react";
import ReactDOM from "react-dom"; //rendering engine
import {Router, Route, IndexRoute, hashHistory} from "react-router";
import Layout from "./pages/Layout";
import ListItems from "./pages/ListItems";
import NewItem from "./pages/NewItem";
import Tests from "./pages/Tests";
import Visuality from "./pages/Visuality";

const app = document.getElementById('app');

ReactDOM.render(
<Router history={hashHistory}>
    <Route path="/" component={Layout}>
        <IndexRoute component={ListItems}></IndexRoute>
        <Route path="new-item" component={NewItem}></Route>
        <Route path="tests" component={Tests}></Route>
        <Route path="visuality" component={Visuality}></Route>
    </Route>
    </Router>, app);