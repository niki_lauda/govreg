import React from "react";
import * as companiesActions from "../actions/CompaniesActions";
import AirCompany from "../components/AirCompany";
import TimerMixin from 'react-timer-mixin';
import url from "../apiRoute";

export default class NewItem extends React.Component {
    constructor() {
        super();
        this.state = AirCompany.getInitialState();
        this.added = false;
    }

    componentWillUnmount() {
        TimerMixin.clearTimeout(this.timer);
    }

    handleFieldChange(e) {
        var object = {};
        object[e.target.name] = e.target.value;
        this.setState(object);
    }

    removeNotification(){
        this.added = false;
    }

    formSubmit() {
        companiesActions.saveCompanyApi(url + '/company', this.state);
        this.setState(AirCompany.getInitialState());
        this.added = true;
        TimerMixin.setTimeout(this.removeNotification.bind(this), 100);
    }

    render() {
        return (
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-center">
                    <div class="form-group jumbotron" style={{width:850, padding: 5}}>
                        <div class="form-group">
                            <label for="decisionDate">Дата рішення про надання/зміну права</label>
                            <input type="input" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.decisionDate} class="form-control" id="decisionDate" name="decisionDate" placeholder="дд.мм.рррр"/>
                        </div>

                        <div class="form-group">
                            <label for="authorityCode">№ права</label>
                            <input type="number" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.authorityCode} class="form-control"
                                   id="authorityCode" name="authorityCode" placeholder="Номер права"/>
                        </div>

                        <div class="form-group">
                            <label for="authorityType">Тип права</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.authorityType} class="form-control" id="authorityType" name="authorityType" placeholder="Тип права"/>
                        </div>

                        <div class="form-group">
                            <label for="carrierCodeName">Назва і код ІКАО / ІАТА авіаперевізника</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.carrierCodeName} class="form-control" id="carrierCodeName" name="carrierCodeName"
                                   placeholder="Українське найменуваня / (Міжнародне)"/>
                        </div>

                        <div class="form-group">
                            <label for="destCountry">Країна призначення</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.destCountry} class="form-control" id="destCountry" name="destCountry" placeholder="Кінцевий пункт"/>
                        </div>

                        <div class="form-group">
                            <label for="route">Маршрут</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)} value={this.state.route}
                                   class="form-control" id="route" name="route" placeholder="Країна вильоту-Транзит"/>
                        </div>

                        <div class="form-group">
                            <label for="flightFrequency">Частота рейсів</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.flightFrequency} class="form-control" id="flightFrequency" name="flightFrequency" placeholder="<число> р/т"/>
                            <p class="help-block">разів/тиждень, або разів/місяць, або разів/рік </p>
                        </div>

                        <div class="form-group">
                            <label for="authorityStartDate">Дата початку дії права</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.authorityStartDate} class="form-control" id="authorityStartDate" name="authorityStartDate" placeholder="дд.мм.рррр"/>
                        </div>

                        <div class="form-group">
                            <label for="confirmationBase">Підстава надання</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.confirmationBase} class="form-control"
                                   id="confirmationBase" name="confirmationBase" placeholder="Тип та номер документу"/>
                        </div>

                        <div class="form-group">
                            <label for="confirmationDate">Дата видачі</label>
                            <input type="text" onChange={this.handleFieldChange.bind(this)}
                                   value={this.state.confirmationDate} class="form-control" id="confirmationDate" name="confirmationDate" placeholder="дд.мм.рррр"/>
                        </div>
                        <button type="submit" onClick={this.formSubmit.bind(this)} class="btn btn-primary btn-lg">
                            Додати
                        </button>
                    </div>
                    <div class="alert alert-success" hidden={!this.added} style={{width:850}}>
                        <strong >Нова Авіакомпанія!</strong> додана до бази даних.
                    </div>
                </div>
            </div>
        );
    }
}