import React from "react";
import CompaniesStore from "../stores/CompaniesStore";
import AirCompany from "../components/AirCompany";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Loader from "../components/Loader";


export default class ListItems extends React.Component {
    constructor() {
        super();
        this.getCompanies = this.getCompanies.bind(this);
        this.state = {
            companies: CompaniesStore.getAll(),
            loading: true
        }
    }

    componentWillMount() {
        CompaniesStore.on("change", this.getCompanies);
    }

    componentWillUnmount() {
        CompaniesStore.removeListener("change", this.getCompanies);
    }

    getCompanies() {
        this.setState({
            companies: CompaniesStore.getAll()
        });
    }

    render() {
        if (typeof this.state.companies === 'undefined') {
            return (<Loader msg="Loading Air Companies from API"/>);
        }

        const {companies} = this.state;
        const companiesComponents = companies.map((company)=> {
            return <AirCompany key={company._id} data={company}/>;
        });

        return (
            <div>
                <div class="jumbotron">
                    <h1><span class="label label-info">{ companies.length }</span>
                         Air Companies in the list</h1>
                </div>
                <div>
                    <ReactCSSTransitionGroup transitionAppear={true} transitionAppearTimeout={500} transitionName="example" transitionEnterTimeout={1000} transitionLeaveTimeout={1000}>
                        {companiesComponents}
                    </ReactCSSTransitionGroup>
                </div>
            </div>
        );
    }
}