import React from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";

export default class Layout extends React.Component {
    constructor() {
        super();
    }

    render() {
        const {location} = this.props;
        return (
            <div>
                <Navbar location={location}/>
                <div class="container" style={{marginTop: "60px"}}>
                    {this.props.children}
                    <Footer/>
                </div>
            </div>
        );
    }
}