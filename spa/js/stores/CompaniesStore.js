import {EventEmitter} from "events";
import dispatcher from "../dispatcher";
import * as companiesActions from "../actions/CompaniesActions";
import url from "../apiRoute";

class CompaniesStore extends EventEmitter{
    constructor(){
        super();
        companiesActions.accessApi(url + '/company');
        this.state = {
            companies : null
        }
    }

    getAll(){
        return this.companies;
    }

    handleActions(action){
        switch(action.type){
            case "TROLOLO":
                console.log("TROLOLO");
                alert("TOTOTOT");
                break;
            case "RECEIVED_COMPANIES":
                this.companies = action.context;
                this.emit("change");
                break;
            case "SAVED_COMPANIES":
                companiesActions.accessApi(url + '/company');
                this.emit("change");
                break;
            case "DELETED_COMPANY":
                companiesActions.accessApi(url + '/company');
                break;
            case "UPDATED_COMPANY":
                companiesActions.accessApi(url + '/company');
                break;
            case "COMPANIES_LOADING":
                console.log("LOADING...............");
                break;

        }
    }
}

const companiesStore = new CompaniesStore;
dispatcher.register(companiesStore.handleActions.bind(companiesStore));
export default companiesStore;